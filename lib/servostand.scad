/*
 * University of Illinois/NCSA
 * Open Source License
 *
 *  Copyright (c) 2007-2008,The Board of Trustees of the University of
 *  Illinois.  All rights reserved.
 *
 *  Copyright (c) 2012 Sam King
 *
 *  Developed by:
 *
 *  Professor Sam King in the Department of Computer Science
 *  The University of Illinois at Urbana-Champaign
 *      http://www.cs.illinois.edu/homes/kingst/Research.html
 *
 *       Permission is hereby granted, free of charge, to any person
 *       obtaining a copy of this software and associated
 *       documentation files (the "Software"), to deal with the
 *       Software without restriction, including without limitation
 *       the rights to use, copy, modify, merge, publish, distribute,
 *       sublicense, and/or sell copies of the Software, and to permit
 *       persons to whom the Software is furnished to do so, subject
 *       to the following conditions:
 *
 *          Redistributions of source code must retain the above
 *          copyright notice, this list of conditions and the
 *          following disclaimers.
 *
 *          Redistributions in binary form must reproduce the above
 *          copyright notice, this list of conditions and the
 *          following disclaimers in the documentation and/or other
 *          materials provided with the distribution.
 *
 *          Neither the names of Sam King, the University of Illinois,
 *          nor the names of its contributors may be used to endorse
 *          or promote products derived from this Software without
 *          specific prior written permission.
 *
 *  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
 *  EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
 *  MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
 *  NONINFRINGEMENT.  IN NO EVENT SHALL THE CONTRIBUTORS OR COPYRIGHT
 *  HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
 *  WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 *  OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER
 *  DEALINGS WITH THE SOFTWARE.
*/

leg_width = 5;
table_height = 30;
table_thickness = 3;
wiggle = .3;
wiggle_x = wiggle;
wiggle_y = wiggle*4;

difference() {	
	cube([60,80,table_thickness]);
	translate([20-wiggle_x,20-wiggle_y,0])
		cube([20+2*wiggle_x,40+2*wiggle_y,table_thickness]);
	translate([25,16,0]) cylinder(r=2, h=table_thickness);
	translate([35,16,0]) cylinder(r=2, h=table_thickness);
	translate([25,64,0]) cylinder(r=2, h=table_thickness);
	translate([35,64,0]) cylinder(r=2, h=table_thickness);
}
translate([0,0,0]) cube([leg_width,leg_width,table_height]);
translate([60-leg_width,0,0]) cube([leg_width,leg_width,table_height]);
translate([60-leg_width,80-leg_width,0]) cube([leg_width,leg_width,table_height]);
translate([0,80-leg_width,0]) cube([leg_width,leg_width,table_height]);
