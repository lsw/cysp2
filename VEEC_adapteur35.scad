//adaptateur tubes fluo IP65 sur pied 

hauteur=20;

difference(){
	union(){
    difference(){
      cylinder(hauteur, r=27.5,center=true,$fn=10);
// on fait un pourtour extérieur grossier pour aller plus vite avec $fn
      cylinder(hauteur+2, r=17.5,center=true,$fn=100);
// on fait un pourtour intérieur +fin pur ne pas devoir ébavurer 
      rotate([90,90,0])
  translate([0,0,-20])
  cylinder(h = 50, r=2.5,center=true,$fn=100);
				}
 difference(){
  translate([0,25,0])
  cube(size=[40,15,hauteur], center=true);
  rotate([90,90,0])
  translate([0,0,-20])
  cylinder(h = 50, r=2.5,center=true,$fn=100);
				}
		}
// pour laisser passer la tête du boulon
 translate([0,-8.5,0])
 rotate([90,0,0])
 cylinder(h =60, r=4.6,center=true,$fn=6);
}
// le rendre traversant en hexagonal à l'opposé pour introduire le boulon