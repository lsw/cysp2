//adaptateur tubes fluo IP65 sur pied 

// ce serait mieux de passer les éléments en module avant de les assembler

hauteur=20;

difference(){
	union(){
    difference(){
// on fait un pourtour extérieur grossier pour aller plus vite avec $fn
      cylinder(hauteur, r=27.5,center=true,$fn=10);
// on fait un pourtour intérieur +fin pour ne pas devoir ébavurer 
      cylinder(hauteur+2, r=17.5,center=true,$fn=100);
				}
		}
}

// double adaptateur tubes fluo IP65 sur pied
  		translate([0,5,0])
 	difference(){
  		translate([0,17.5,0])
  		//la barre 
  		cube([150,10,hauteur],center=true);
  		//ses perçages
		rotate([90,0,0])
		translate([-60,0,-50])
  		cylinder(hauteur*4,r=2.2,$fn=100);
		rotate([90,0,0])
		translate([60,0,-50])
  		cylinder(hauteur*4,r=2.05,$fn=100);
		}