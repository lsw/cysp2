// double adaptateur tubes fluo IP65 sur pied 
hauteur=20;
difference(){
	union(){
	difference(){
		// on fait un pourtour extérieur plus grossier
      	cylinder(hauteur, r=15,center=true,$fn=6);
		//défonce pour la vis verticale
      	cylinder(hauteur+2, r=3.25,center=true,$fn=100);
		}
 	difference(){
  		translate([0,17.5,0])
  		//la barre 
  		cube([150,10,hauteur],center=true);
  		//ses perçages
		rotate([90,0,0])
		translate([-60,0,-50])
  		cylinder(hauteur*4,r=2.2,$fn=100);
		rotate([90,0,0])
		translate([60,0,-50])
  		cylinder(hauteur*4,r=2.05,$fn=100);
		}
}
}