//adaptateur tubes fluo IP65 sur pied 
hauteur=20;
difference(){
union(){
    difference(){
      cylinder(hauteur, r=15,center=true,$fn=10);
// on fait un pourtour extérieur plus grossier
      cylinder(hauteur+2, r=3.25,center=true,$fn=100);
//défonce pour la vis verticale
      rotate([90,90,0])
  translate([0,0,-20])
  cylinder(h = 50, r=2.5,center=true,$fn=100);
//défonce pour la vis horizontale raccord des tubes fluo
}
 difference(){
  translate([0,25,0])
  cylinder(hauteur, r=15,center=true,$fn=10);
//l'embase
  rotate([90,90,0])
  translate([0,0,0])
  cylinder(h = 100, r=2.5,center=true,$fn=100);
}
}
 translate([0,-12,0])
 rotate([90,0,0])
  cylinder(h =40, r=4.8,center=true,$fn=6);}
// rayon passé à 4.6 pour ne pas devoir faire d'ajustage avec les têtes des vis en 5mm
// le rendre traversant en hexagonal à l'opposé du carré pour introduire le boulon