// Libre Stage Works 
// support avec manchon évidé section de 12 pour servomoteur et roulement à bille
// disposer à 45° avec Slic3r pour s'adapter au plateau de la Folda/RepRap
// OSHW licence, copyright Olivier Heinry, 2012 

module servoAdapt(){
height = 5;
rayon = 1.9;
difference() {	
	cube([40,60,height],center=true);
	// comme j'ai ajouté un 1mm de pourtour pour ne pas avoir à réusiner la pièce, il faut enlever ce 1mm/2 au décalage avant de défoncer avec le second cube
		cube([21,41,height*1.3], center=true);
	translate([5,25,-5]) cylinder(rayon, h=height*2,$fn=180);
	translate([-5,25,-5]) cylinder(rayon, h=height*2,$fn=180);
	translate([-5,-25,-5]) cylinder(rayon, h=height*2,$fn=180);
	translate([5,-25,-5]) cylinder(rayon, h=height*2,$fn=180);
	}
}

module porteBille() {
	hauteur=10;
	difference(){
		translate([0,43,0]) cube([40,30,hauteur/2], center=true);
		//chanfreins
		translate([-22,53,0]) rotate([0,0,60]) cube([40,20,hauteur],center=true);
		translate([22,53,0]) rotate([0,0,-60]) cube([40,20,hauteur],center=true);
	}
	translate([0,45,0])
	difference() {
		cylinder(r=15,h=hauteur*1.2,$fn=6);
		translate(0,hauteur/2,0);
		cylinder(r=5.5,h=hauteur,$fn=180);
		}
	
}
module manchon(){
	section=12;
	difference(){
		cube([section,50,section], center=true);
		cube([section*1.2,40,4], center=true);
	}
}

//on colle tout ça ensemble	
union(){
servoAdapt();
porteBille();
translate([0,-55,3.5]) manchon();
}
