//couronne de transmission roue cysp 2
//adaptée au servo Y du miroir

difference(){
//pignon denté
gear_wheel(re=10,ri=7,n=12,d=0.5,h=5,t=0.1);
cylinder(h = 0, r=1.45,center=true,$fn=100);
}//fin pignon

translate([0,41,0])
rotate([0,0,5])
color([1,1,0.5])

difference(){
//roue dentée
gear_wheel(re=30,ri=27,n=36,d=0.5,h=5,t=0.1);
//trou central
union(){
   cylinder(h = 60, r=1.5,center=true,$fn=80);
   translate ([0,15,0])
   cylinder(h = 20, r=8,center=true);
   rotate ([0,0,120])
   translate ([0,15,0])
   cylinder(h = 20, r=8,center=true);
   rotate ([0,0,240])
   translate ([0,15,0])
   cylinder(h = 60, r=8,center=true);
}//fin union
}//fin roue dentée